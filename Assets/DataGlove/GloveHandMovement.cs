﻿
using UnityEngine;
using System.Collections;
using System;

public class GloveHandMovement : MonoBehaviour {
	public Transform[] indexFinger = new Transform[3];
	public Transform[] middleFinger = new Transform[3];
	public Transform[] ringFinger = new Transform[3];
	public Transform[] pinkyFinger = new Transform[3];
	public Transform[] thumbFinger = new Transform[3];
	public Transform hand;
	public Transform wrist;

	void Start () {
	
	}
	
	float bbb = 0;

	// Update is called once per frame
	void Update () {
		float[,] idx;
		float[,] middle;
		float[,] ring;
		float[,] pinky;
		float[,] thumb;
		float[,] handValue;

		int a = 1; // Roll
		int b = 2; // Yaw
		int c = 0; // Pitch

		float aa = -1;
		float bb = -1;
		float cc = 1;
		
		Quaternion defaultRot = new Quaternion (0.7f, 0f, -0.7f, 0f);

		idx = GloveData.getFingerValues (GloveData.FINGER_IDX_1, GloveData.HAND_IDX_RIGHT);
		middle = GloveData.getFingerValues (GloveData.FINGER_IDX_2, GloveData.HAND_IDX_RIGHT);
		ring = GloveData.getFingerValues (GloveData.FINGER_IDX_3, GloveData.HAND_IDX_RIGHT);
		pinky = GloveData.getFingerValues (GloveData.FINGER_IDX_4, GloveData.HAND_IDX_RIGHT);
		thumb = GloveData.getFingerValues (GloveData.FINGER_IDX_THUMB, GloveData.HAND_IDX_RIGHT);
		handValue = GloveData.getFingerValues (GloveData.FINGER_IDX_HAND, GloveData.HAND_IDX_RIGHT);



		// Compensate hand's yaw.
		if (Input.GetKey (KeyCode.LeftBracket))
			bbb += 20 * Time.deltaTime;
		else if (Input.GetKey (KeyCode.RightBracket))
			bbb -= 20 * Time.deltaTime;
		else if (Input.GetKey (KeyCode.Space))
			bbb = handValue [1, 2];


		wrist.rotation = Quaternion.Euler( defaultRot.eulerAngles + new Vector3(handValue [1, a] * aa, handValue[1,b]*bb + bbb, handValue[1,c]*cc) );
		hand.rotation = Quaternion.Euler(  defaultRot.eulerAngles + new Vector3(handValue [0, a] * aa, handValue[0,b]*bb + bbb, handValue[0,c]*cc) );

		for (int i=0; i<3; i++) { 

			Vector3 idxRot = new Vector3 (idx[i,a]*aa, idx[i,b]*bb + bbb, idx[i,c]*cc );
			Vector3 middleRot = new Vector3(middle[i,a]*aa, middle[i,b]*bb + bbb, middle[i,c]*cc);
			Vector3 ringRot = new Vector3(ring[i,a]*aa, ring[i,b]*bb + bbb, ring[i,c]*cc);
			Vector3 pinkyRot = new Vector3(pinky[i,a]*aa, pinky[i,b]*bb + bbb, pinky[i,c]*cc);
			Vector3 thumbRot = new Vector3(thumb[i,a]*aa, thumb[i,b]*bb + bbb, thumb[i,c]*cc);

			indexFinger [i].rotation = Quaternion.Euler( defaultRot.eulerAngles + idxRot );
			ringFinger [i].rotation = Quaternion.Euler( defaultRot.eulerAngles + ringRot );
			middleFinger [i].rotation = Quaternion.Euler( defaultRot.eulerAngles + middleRot );
         	pinkyFinger [i].rotation = Quaternion.Euler( defaultRot.eulerAngles + pinkyRot );
            thumbFinger[i].rotation = Quaternion.Euler( defaultRot.eulerAngles + thumbRot );
		}
	}

}
