﻿using UnityEngine;
using System.Collections;

public class MagicScript : MonoBehaviour {
	public Transform effect = null;
	public Transform hand = null;
	
	public Transform fireEffect;
	private ArrayList firedEffects;

	private bool handCloseState = false;
	private readonly float FIRE_SPEED = 4f;

	// Use this for initialization
	void Start () {
		firedEffects = new ArrayList ();
	}
	
	// Update is called once per frame
	void Update () {

		effect.position = hand.position;
		effect.rotation = hand.rotation;

		if (effect != null) {
			if (GloveData.getTotalBending (GloveData.HAND_IDX_RIGHT) > 600) {
				effect.gameObject.SetActive (true);
				if(handCloseState == false){
					handCloseState = true;
				}

			} else {
				effect.gameObject.SetActive (false);

				if(handCloseState == true){
					handCloseState = false;
					addFire ();
				}
			}
		}

		foreach( Transform firedOne in firedEffects)
		{
			firedOne.Translate(new Vector3(-FIRE_SPEED*Time.deltaTime, 0.0f, 0.0f));

			
			float flyDist = Mathf.Abs(firedOne.position.x) + Mathf.Abs(firedOne.position.y) + Mathf.Abs(firedOne.position.z);
			
			if(flyDist > 20.0f){
				Destroy (firedOne.gameObject);
				firedEffects.Remove(firedOne);
			}
		}

	}

	private void addFire(){
		firedEffects.Add ( Instantiate (fireEffect, hand.position, hand.rotation) );
	}
}
