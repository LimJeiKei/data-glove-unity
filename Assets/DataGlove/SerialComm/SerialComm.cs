﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Threading;
using System;

public class SerialComm : MonoBehaviour {
	public bool isConnect = true;

//	static SerialPort port = new SerialPort("\\\\.\\COM5", 115200);
	static SerialPort port = new SerialPort("\\\\.\\COM26", 115200);
//	static SerialPort port = new SerialPort ("/dev/tty.DG_R_A587-SPPDev");
//	static SerialPort port = new SerialPort("/dev/tty.DG2_R_A5D8-SPPDev");

	static bool isConnected;
	static HandParser parser;
	public static byte[] ACK_MSG = new byte[]{0xFA, 0xA1, 0x00, 0x00};

	private float ackTime = 0;

	// Use this for initialization
	void Start () {
		port.DataBits = 8;
		port.StopBits = StopBits.One;
		port.Parity = Parity.None;
		port.Handshake = Handshake.None;

		parser = new HandParser (256);
	
		if (isConnect) {
			Debug.Log ("START");

			new Thread (connect).Start ();
		}
	}

	public static void myCopy(Array src, int srcOffset, Array dst, int dstOffset, int count)
	{
		Buffer.BlockCopy (src, srcOffset * 4, dst, dstOffset * 4, count * 4);
	}
	
	// Update is called once per frame
	void Update () {
		if (port.IsOpen == false) {
			isConnected = false;
		}

		ackTime += Time.deltaTime;

		if (ackTime > 0.9f && isConnected) {
			ackTime = 0;
//			Debug.Log ("ACK!");
			port.Write(ACK_MSG, 0, ACK_MSG.Length);
		}

		if (Input.GetKeyDown (KeyCode.Q)) {
			isConnected = false;
			port.Close ();
			Debug.Log ("Disconnect");
		}
		else if(Input.GetKeyDown(KeyCode.B)){
			Debug.Log ("Set bias!");
		}
	}
	
	void OnDestroy(){
		isConnected = false;

		if (port != null) {
			port.Close ();
			port.Dispose();
		}
	}

	public static void connect(){

		Debug.Log ("Openning the port " + port.PortName);
		port.Open();
		Debug.Log ("is port Connected : " + port.IsOpen.ToString() );

		isConnected = port.IsOpen;
		if(isConnected)
			new Thread(readData).Start ();
	}

	private static void dataReceivedHandler(object sender, SerialDataReceivedEventArgs e){
		Debug.Log ("Data received!");
	}

	public static void readData()
	{
		Debug.Log ("Read data");

		while (isConnected) 
		{
			try{
				parser.parseMessage(new byte[]{(byte)port.ReadByte()});
			} catch(TimeoutException){
				Debug.Log ("Time out exception");
			}
		}
	}

	public static string byteToHexString(byte[] data){
		string result = "";

		for (int i=0; i<data.Length; i++) {
			result += byteToChar((byte)((data[i] & 0xF0) >> 4));
			result += byteToChar((byte)(data[i] & 0x0F)) + " ";
		}

		return result;
	}

	public static char byteToChar(byte v){
		if(v >= 0x00 && v <= 0x09)
			return (char)(v+0x30);
		else
			return (char)(v+0x37);
	}

}
