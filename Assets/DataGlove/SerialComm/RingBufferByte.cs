
/**
 * Ring Buffer for byte class.<br /><br />
 * 
 * Version 1.0 (2014. 04. 13)
 * 
 * @author JeiKei ( Lim.JeiKei@gmail.com )
 *
 */

public class RingBufferByte {
	/**
	 * Buffer size.
	 */
	private readonly int BUF_SIZE;
	/**
	 * Buffer data
	 */
	private byte[] buffer;
	
	/**
	 * Start index of ring buffer
	 */
	private int startIdx;
	/**
	 * End index of ring buffer
	 */
	private int endIdx;
	
	/**
	 * 
	 * Generate RingBufferByte class.<br />
	 * start index and end index will be initialized with the number of 0.
	 * 
	 * @param bufferSize
	 * 	-	 Buffer size of ring buffer. <br />
	 * 		This will be the size of byte array.
	 */
	public RingBufferByte(int bufferSize){
		BUF_SIZE = bufferSize;
		
		buffer = new byte[BUF_SIZE];
		startIdx = 0;
		endIdx = 0;
	}

	/**
	 * Put data into ring buffer.<br />
	 * End index will be located at the end of the data.
	 * 
	 * @param data
	 * 
	 */
	public void putData(byte[] data){
		for(int i=0; i<data.Length; i++){
			buffer[endIdx] = data[i];
			
			endIdx = ((i-1)==data.Length) ? endIdx : goNextIdx(endIdx, 1, BUF_SIZE);
		}
	}
	/**
	 * Notify that you have consumed the data from ring buffer.<br />
	 * Start index will be forwarded to the size.
	 * 
	 * @param size
	 * The size of the data you have comsumed.
	 */
	public void consumeData(int size){
		startIdx = goNextIdx(startIdx, size, BUF_SIZE);
	}
	
	/**
	 * Get buffered data.<br />
	 * Its data will be ranged from start index to end index.
	 * 
	 * @return
	 *  - Data ranged from start index to end index.
	 */
	public byte[] getBufferedData(){
		int dataSize = (startIdx > endIdx) ? (BUF_SIZE-startIdx) + endIdx : endIdx - startIdx;
		byte[] result = new byte[dataSize];
		int tmpIdx = startIdx;
		
		for(int i=0; i<dataSize; i++){
			result[i] = buffer[tmpIdx];
			tmpIdx = goNextIdx(tmpIdx, 1, BUF_SIZE);
		}
		
		return result;
	}
	
	/**
	 * Get Current buffer size.
	 * @return
	 */
	public int getBufferSize(){
		return BUF_SIZE;
	}
	/**
	 * Get current start index value.
	 * @return
	 */
	public int getStartIdx(){
		return startIdx;
	}
	/**
	 * Get current end index value.
	 * @return
	 */
	public int getEndIdx(){
		return endIdx;
	}
	
	/**
	 * Move to next point of ring buffer considering its maximum size which is buffer size.
	 * 
	 * @param currentValue - Current index value.
	 * @param step - Steps you wish to move the index.
	 * @param maxValue - Buffer size.
	 * @return
	 *  - It will be (currentValue+step) % maxValue
	 */
	private int goNextIdx(int currentValue, int step, int maxValue){		
		return (currentValue+step) % maxValue;
	}

}
