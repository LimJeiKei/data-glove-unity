﻿using UnityEngine;
using System.Collections;

public class OVR_dataGlove : MonoBehaviour {

	public Transform ovrCamera = null;
	public Transform hand = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (ovrCamera != null && hand != null) {
			Quaternion tmpRot = hand.rotation;

			hand.position = ovrCamera.position;
//			hand.rotation = Quaternion.Euler(new Vector3(0.0f, ovrCamera.rotation.eulerAngles.y, 0.0f));
			hand.rotation = ovrCamera.rotation;
			hand.Translate(new Vector3(0.3f, 0f, 1.8f));
			hand.rotation = tmpRot;


		}
	}
}
