
/**
 * Glove Data class
 * 
 * @author JeiKei ( Lim.JeiKei@gmail.com )
 *
 */
using System;
using UnityEngine;


public class GloveData {
	
	public const int HAND_IDX_LEFT = 0;
	public const int HAND_IDX_RIGHT = 1;
	
	public const int FINGER_IDX_THUMB = 0;
	public const int FINGER_IDX_1 = 1;
	public const int FINGER_IDX_2 = 2;
	public const int FINGER_IDX_3 = 3;
	public const int FINGER_IDX_4 = 4;
	public const int FINGER_IDX_HAND = 5;
	
	
	private static float[] gloveLData = new float[51];
	private static float[] gloveRData = new float[51];

	public static bool isClickedLeft = false;
	public static bool isClickedRight = false;
	
	private const float DATA_ALPHA = 0.1f;
	private const float DATA_BETA =  1 - DATA_ALPHA;
	
	private static float[,] computedLData = new float[17, 3];
	private static float[,] prevComputedLData = new float[17, 3];
	
	private static float[,] biasLData = new float[17, 3];
	
	public static void setHandValue(int fingerIdx, int value, int handIdx)    
	{
		if (handIdx == HAND_IDX_LEFT) {
			gloveLData [fingerIdx] = (float)value/(float)100;
		} else {
			gloveRData [fingerIdx] = (float)value/(float)100;
		}
	}

	/**
	 * Get raw data from the glove.
	 */
	public static float[,] getFingerValues(int fingerIdx, int handIdx){
		float[,] value = null;
		float[] gloveData = null;

		if (handIdx == HAND_IDX_LEFT)
			gloveData = gloveLData;
		else
			gloveData = gloveRData;

		if(fingerIdx == FINGER_IDX_HAND)
			value = new float[2, 3];
		else
			value = new float[3, 3];
		
		switch(fingerIdx){
		case FINGER_IDX_THUMB:
			for(int i=0; i<3; i++){
				value[0,i] = gloveData[3+i];
				value[1,i] = gloveData[18+i];
				value[2,i] = gloveData[33+i];
			}
			break;
		case FINGER_IDX_HAND:
			for(int i=0; i<3; i++){
				value[0,i] = gloveData[48+i];
				value[1,i] = gloveData[i];
			}
			break;
		case FINGER_IDX_1:
			for(int i=0; i<3; i++){
				value[0,i] = gloveData[6+i];
				value[1,i] = gloveData[21+i];
				value[2,i] = gloveData[36+i];
			}
			break;
		case FINGER_IDX_2:
			for(int i=0; i<3; i++){
				value[0,i] = gloveData[9+i];
				value[1,i] = gloveData[24+i];
				value[2,i] = gloveData[39+i];
			}
			break;
		case FINGER_IDX_3:
			for(int i=0; i<3; i++){
				value[0,i] = gloveData[12+i];
				value[1,i] = gloveData[27+i];
				value[2,i] = gloveData[42+i];
			}
			break;
		case FINGER_IDX_4:
			for(int i=0; i<3; i++){
				value[0,i] = gloveData[15+i];
				value[1,i] = gloveData[30+i];
				value[2,i] = gloveData[45+i];
			}
			break;
		}

		// Estimate the finger tip's rotation
		// Since current version of data glove has only 2 sensors on finger, this procedure is required.
		if (fingerIdx != FINGER_IDX_HAND && fingerIdx != FINGER_IDX_THUMB) {
			for(int i=0; i<3; i++){
				if(i == 0)
					value[2,i] = (value[1,i]-value[0,i])*0.75f + value[1,i];
				else
					value[2,i] = value[1,i];
			}
		}

		
		return value;
	}

	public static float getBending(int fingerIdx, int handIdx){
		float[,] data;
		float result = 0;

		data = getFingerValues (fingerIdx, handIdx);

		for (int i=1; i<3; i++) {
			result += Mathf.Abs( data[i,0] - data[i-1,0] );
		}

		return result;
	}

	public static float[] getBending(int handIdx){
		float[,] data;
		float[] result = new float[5];

		for (int i=0; i<5; i++) {
			result[i] = getBending (i, handIdx);
		}

		return result;
	}
	public static float getTotalBending(int handIdx){
		float[] data = getBending (handIdx);
		float result = 0;

		for (int i=0; i<5; i++)
			result += data [i];

		return result;
	}
}
