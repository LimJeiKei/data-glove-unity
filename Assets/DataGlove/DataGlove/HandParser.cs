

/**
 * Message parser for data glove.
 * 
 * @author JeiKei ( Lim.JeiKei@gmail.com)
 *
 */
using UnityEngine;


public class HandParser {
	private const byte HEADER1 = (byte) 0xFA;
	private const int DATA_SIZE = 0x06;
	
	private RingBufferByte buffer;
	
	/**
	 * Create message parser class.<br />
	 * 
	 * @param buffersize
	 *  - Ring buffer size.
	 */
	public HandParser(int buffersize){
		buffer = new RingBufferByte(buffersize);
	}
	
	/**
	 * Starts parse message. <br />
	 * msg will be stored in ring buffer and decapsulized.<br />
	 * If finding proper protocol message succeeded, it will be passed to processData(byte[] data)<br />
	 * 
	 * @param msg
	 */
	public void parseMessage(byte[] msg){

		buffer.putData(msg);

		byte checksum = 0x00;
		
		int i = 0;
		byte[] data = buffer.getBufferedData();

//		Debug.Log ("parseMsg :: " + SerialComm.byteToHexString (data));

		while(i+3+DATA_SIZE < data.Length){	// Check if data to be parsed have enough size.	
			
			if((0xFF&data[i]) == ((0xff)&HEADER1) && (0xFF&data[i+2])==((0xFF)&DATA_SIZE )){	// Check header
				
				checksum = 0x00;
				for(int j=i+3; (j<i+3+DATA_SIZE) && (j<data.Length); j++){	// Compute checksum
					checksum =  (byte) ((checksum&0xFF) ^ (0xFF&data[j]));
				}
				if((0xFF&checksum) == (0xFF&data[i+3+DATA_SIZE])){	// Check checksum
					buffer.consumeData(DATA_SIZE+4);		// Consume the data
					putGloveData(new byte[]{data[i], data[i+1], data[i+2], data[i+3], data[i+4], data[i+5], data[i+6], data[i+7], data[i+8]});
					
					data = buffer.getBufferedData();		// Get new data
					i = 0;	// Reset index
					continue;
				}
				else{	// if 0xFF&chesum ...
				}
			}
			i++;		//Increase index
			
		} // while true
		
		if(false){	// if debug printing or not
			string tmp = "";
			float[,] v0 = GloveData.getFingerValues(GloveData.FINGER_IDX_HAND, GloveData.HAND_IDX_RIGHT);
			float[,] v1 = GloveData.getFingerValues(GloveData.FINGER_IDX_THUMB, GloveData.HAND_IDX_RIGHT);
			float[,] v2 = GloveData.getFingerValues(GloveData.FINGER_IDX_1, GloveData.HAND_IDX_RIGHT);
			float[,] v3 = GloveData.getFingerValues(GloveData.FINGER_IDX_2, GloveData.HAND_IDX_RIGHT);
			
			for(int j=0; j<v2.GetLength(0); j++){
				for(int k=0; k<v2.GetLength(1); k++){
					tmp += v2[j,k] + " / ";
				}
			}
			for(int j=0; j<v3.GetLength(0); j++){
				for(int k=0; k<v3.GetLength(1); k++){
					tmp += v3[j,k] + " / ";
				}
			}
			for(int j=0; j<v1.GetLength(0); j++){
				for(int k=0; k<v1.GetLength(1); k++){
					tmp += v2[j,k] + " / ";
				}
			}
			for(int k=0; k<v0.GetLength(1); k++){
				tmp += v0[1,k] + " / ";
			}

			Debug.Log("Hand :: " + tmp + "\t :::: " + GloveData.isClickedRight.ToString());
		}
	}
	
	void putGloveData(byte[] data){
		int index = 0xff;
		int type = 0xff;

		byte x = (byte)(data [1] & 0xFF);

		switch (x) {
			// Left hand
			case 0x50:	index = 0;	type = 1;	break;
			case 0x51:	index = 3;	type = 1;	break;
			case 0x52:	index = 6;	type = 1;	break;
			case 0x53:	index = 9;	type = 1;	break;
			case 0x54:	index = 12;	type = 1;	break;
			case 0x55:	index = 15;	type = 1;	break;
			case 0x56:	index = 18;	type = 1;	break;
			case 0x57:	index = 21;	type = 1;	break;
			case 0x58:	index = 24;	type = 1;	break;
			case 0x59:	index = 27;	type = 1;	break;
			case 0x5A:	index = 30;	type = 1;	break;
			case 0x5B:	index = 33;	type = 1;	break;
			case 0x5C:	index = 36;	type = 1;	break;
			case 0x5D:	index = 39;	type = 1;	break;
			case 0x5E:	index = 42;	type = 1;	break;
			case 0x5F:	index = 45;	type = 1;	break;
			case 0x60:	index = 48;	type = 1;	break;
			case 0x61:
				if(data[3] == 0x00)
					GloveData.isClickedLeft = false;
				else if(data[3] == 0x01)
					GloveData.isClickedLeft = true;
				break;

			// Right hand
			case 0x70:	index = 0;	type = 2;	break;
			case 0x71:	index = 3;	type = 2;	break;
			case 0x72:	index = 6;	type = 2;	break;
			case 0x73:	index = 9;	type = 2;	break;
			case 0x74:	index = 12;	type = 2;	break;
			case 0x75:	index = 15;	type = 2;	break;
			case 0x76:	index = 18;	type = 2;	break;
			case 0x77:	index = 21;	type = 2;	break;
			case 0x78:	index = 24;	type = 2;	break;
			case 0x79:	index = 27;	type = 2;	break;
			case 0x7A:	index = 30;	type = 2;	break;
			case 0x7B:	index = 33;	type = 2;	break;
			case 0x7C:	index = 36;	type = 2;	break;
			case 0x7D:	index = 39;	type = 2;	break;
			case 0x7E:	index = 42;	type = 2;	break;
			case 0x7F:	index = 45;	type = 2;	break;
			case 0x80:	index = 48;	type = 2;	break;
			case 0x81:
				if(data[3] == 0x00)
					GloveData.isClickedRight = false;
				else if(data[3] == 0x01)
					GloveData.isClickedRight = true;
				break;
			default:
//				Debug.Log ("data[1] : " + x);
				break;
		}

		if(index == 0xFF && type == 0xFF)
			return;
		
		for(int i=0; i<3; i++){
			int output=(((data[2*i + 4]&0xFF)<<8) |	(data[2*i + 3]&0xFF));
			
			GloveData.setHandValue(index + i, (output>0x7FFF ? -(0xFFFF-output) : output), type);
		}
		
	}
	
	
	
}
