﻿using UnityEngine;
using System.Collections;

public class SensorDataDebug : MonoBehaviour {
	GUIText debugTxt;

	// Use this for initialization
	void Start () {
		debugTxt = transform.GetComponent<GUIText>();
	}
	
	// Update is called once per frame
	void Update () {
		string tmp = "";
		float[,] v0;
		float[,] v1;
		float[,] v2;
		float[,] v3;
		float[,] v4;
		float[,] v5;
		bool isClickedRight = false;

		v0 = GloveData.getFingerValues (GloveData.FINGER_IDX_HAND, GloveData.HAND_IDX_RIGHT);
		v1 = GloveData.getFingerValues (GloveData.FINGER_IDX_THUMB, GloveData.HAND_IDX_RIGHT);
		v2 = GloveData.getFingerValues (GloveData.FINGER_IDX_1, GloveData.HAND_IDX_RIGHT);
		v3 = GloveData.getFingerValues (GloveData.FINGER_IDX_2, GloveData.HAND_IDX_RIGHT);
		v4 = GloveData.getFingerValues (GloveData.FINGER_IDX_3, GloveData.HAND_IDX_RIGHT);
		v5 = GloveData.getFingerValues (GloveData.FINGER_IDX_4, GloveData.HAND_IDX_RIGHT);

		
//		for(int j=0; j<v2.GetLength(0); j++){
//			for(int k=0; k<v2.GetLength(1); k++){
//				tmp += v2[j,k] + "\t/\t";
//			}
//		}
//		tmp += "\n";
//		for(int j=0; j<v3.GetLength(0); j++){
//			for(int k=0; k<v3.GetLength(1); k++){
//				tmp += v3[j,k] + "\t/\t";
//			}
//		}
//		tmp += "\n";
//		for(int j=0; j<v4.GetLength(0); j++){
//			for(int k=0; k<v4.GetLength(1); k++){
//				tmp += v4[j,k] + "\t/\t";
//			}
//		}
//		tmp += "\n";
//		for(int j=0; j<v5.GetLength(0); j++){
//			for(int k=0; k<v5.GetLength(1); k++){
//				tmp += v5[j,k] + "\t/\t";
//			}
//		}
//		tmp += "\n";
//		for(int j=0; j<v1.GetLength(0); j++){
//			for(int k=0; k<v1.GetLength(1); k++){
//				tmp += v2[j,k] + "\t/\t";
//			}
//			tmp += "\n";
//		}
//		tmp += "\n";
//		for (int j=0; j<v0.GetLength(0); j++) {
//			for (int k=0; k<v0.GetLength(1); k++) {
//				tmp += v0 [j, k] + "\t/\t";
//			}
//		}
		float[] flexionFinger = GloveData.getBending(GloveData.HAND_IDX_RIGHT);
		float totalBend = GloveData.getTotalBending(GloveData.HAND_IDX_RIGHT);
		for (int i=0; i<5; i++) {
			tmp += flexionFinger [i] + "\n";
		}
		tmp += totalBend;
		tmp += "\n" + GloveData.isClickedRight;

		debugTxt.text = tmp;
		Debug.Log (tmp); 
	}
}
